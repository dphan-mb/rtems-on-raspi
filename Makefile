# C source names
C_SRC_PATH = src
C_PIECES=$(C_SRC_PATH)/tasks/rtems $(C_SRC_PATH)/svc/led
C_FILES=$(C_PIECES:%=%.c)
C_O_FILES=$(C_PIECES:%=${ARCH}/%.o)

# C header names
H_FILES=$(C_SRC_PATH)/svc/led

SRCS=$(C_FILES)
OBJS=$(C_O_FILES)

# Program names
PGMS=${ARCH}/rtems-on-raspi.exe

include $(RTEMS_MAKEFILE_PATH)/Makefile.inc
include $(RTEMS_CUSTOM)
include $(RTEMS_ROOT)/make/leaf.cfg

# TFTP path
TFTP_PATH = /srv/tftp/

# U-Boot binary build path
UBOOT_BIN_PATH = ./u-boot-bin

.PHONY: uboot all path 

uboot: 
	mkdir -p $(UBOOT_BIN_PATH)
	cd u-boot && make rpi_defconfig CROSS_COMPILE=arm-linux-gnueabi- && make CROSS_COMPILE=arm-linux-gnueabi-
	cp u-boot/u-boot.bin $(UBOOT_BIN_PATH)
	mkimage -A arm -O linux -T script -C none -d u-boot-cmd/boot_from_tftp.scr $(UBOOT_BIN_PATH)/boot.scr.uimg

path: 
	mkdir -p ${ARCH}/$(C_SRC_PATH)/tasks
	mkdir -p ${ARCH}/$(C_SRC_PATH)/svc

all:  path ${ARCH} $(SRCS) $(PGMS)

# The following links using C rules.
$(PGMS): ${OBJS}
	$(make-exe)
	arm-rtems5-objcopy -Obinary $(PGMS) ${ARCH}/kernel.img
	mkimage -A arm -O linux -T kernel -a 0x200000 -e 0x200080 -d ${ARCH}/kernel.img -C none ${ARCH}/rtems.img
	cp ${ARCH}/rtems.img $(TFTP_PATH)
	