#pragma once
#include <stdio.h>
#include <stdlib.h>

void ledInit(void);

void ledOn(void);

void ledOff(void);
