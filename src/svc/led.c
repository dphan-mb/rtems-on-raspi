#include <rtems.h>
#include <stdio.h>
#include <stdlib.h>
#include <bsp.h>
#include "led.h"

void ledInit(void)
{
    *(unsigned int *)BCM2835_GPIO_GPFSEL1 |= 1 << 18;
}

void ledOn(void)
{
    *(unsigned int *)BCM2835_GPIO_GPCLR0 = 1 << 16;
}

void ledOff(void)
{
    *(unsigned int *)BCM2835_GPIO_GPSET0 = 1 << 16;
}

